namespace hellotisiweb.Helpers;

using System.Diagnostics;
using System.Text;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Buffers;
using mkcs.libtisiweb;
using mkcs.libtisiweb.Models;
using mkcs.libtisiweb.Repositories;

public class TisiWebHtmlHelper<T> : HtmlHelper<T>
{
    private readonly ITisiwebAppService _tisiwebAppService;
    private IRepositoryManager _repositoryManager;
    private readonly IEnumerable<Category> _categories;

    public TisiWebHtmlHelper(IHtmlGenerator htmlGenerator, ICompositeViewEngine viewEngine, IModelMetadataProvider metadataProvider, IViewBufferScope bufferScope, HtmlEncoder htmlEncoder, UrlEncoder urlEncoder, ModelExpressionProvider modelExpressionProvider, ITisiwebAppService tisiwebAppService)
        : base(htmlGenerator, viewEngine, metadataProvider, bufferScope, htmlEncoder, urlEncoder, modelExpressionProvider)
    {
        _tisiwebAppService = tisiwebAppService ?? throw new ArgumentNullException(nameof(tisiwebAppService));
        _repositoryManager = _tisiwebAppService.RepositoryManager;
        _categories = _repositoryManager.GetCategories();
    }

    public string Foo(string subset) => "subset=" + subset;

    public static string GetString(IHtmlContent content)
    {
        using var writer = new StringWriter();
        content.WriteTo(writer, HtmlEncoder.Default);
        return writer.ToString();
    }

    public string CreateCodeForNavigationForSubset(string subset)
        => CreateCodeForNavigation(_categories, subset);

    public string CreateCodeForNavigation(IEnumerable<Category> categories, string subset)
    {
        StringBuilder stringBuilder = new();
        foreach (var category in categories)
        {
            ProcessCategory(stringBuilder, category, subset);
        }

        return stringBuilder.ToString();
    }

    private void ProcessCategory(StringBuilder stringBuilder, Category category, string subset, bool isSubCategory = false)
    {
        var hasSubCategories = category.Categories.Any();
        var liClassName = hasSubCategories ? "dropdown" : "nav-item";
        if (hasSubCategories)
        {
            var linkText = GetCategoryText(category, subset);
            stringBuilder.AppendLine("<li class=\"dropdown\">");
            stringBuilder.AppendLine($"<a href=\"#\" class=\"dropdown-toggle nav-link text-dark\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">{linkText}<span class=\"caret\"></span></a>");
            stringBuilder.AppendLine("<ul class=\"dropdown-menu\">");
            foreach (var subcategory in category.Categories)
            {
                ProcessCategory(stringBuilder, subcategory, subset, true);
            }
            stringBuilder.AppendLine("</ul></li>");
        }
        else
        {
            if (isSubCategory)
            {
                stringBuilder.AppendLine("<li>");
            }
            else
            {
                stringBuilder.AppendLine($"<li class=\"{liClassName}\">");
            }
            Dictionary<string, string> routeValues = new()
            {
                { "subset", subset }
            };
            if (category.Id is not null)
            {
                routeValues.Add("id", category.Id);
            }

            var linkText = GetCategoryText(category, subset);
            Trace.WriteLine($"{category.Controller}.{category.Action}({category.Id}) fragment {category?.Description?.Name ?? ""} with text {linkText}");
            var link = GetString(ActionLink(linkText, category?.Action ?? "", category?.Controller ?? "", "https", "", "", routeValues, new { @class = "nav-link text-dark" }));
            stringBuilder.AppendLine(link.Replace("#\"", "\""));
            stringBuilder.AppendLine("</li>");
        }
    }

    private string GetCategoryText(Category category, string subset)
    {
        var fragment = category.Description;
        if (fragment is null)
        {
            return "Description is null";
        }

        if (!fragment.ContainsSubset(subset))
        {
            return $"Subset \"{subset}\" not available for fragment \"{fragment.Name ?? string.Empty}\"";
        }

        return fragment[subset];
    }
}