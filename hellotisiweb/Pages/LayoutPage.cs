using Microsoft.AspNetCore.Mvc.Razor;

public abstract class LayoutPage<dynamic> : RazorPage<dynamic>
{
    public string SiteName { get; } = "hellotisiweb";

    public string ModelType => Model?.GetType().FullName ?? "null";
        //=> $"<a class=\"nav-link text-dark\" asp-area=\"\" asp-controller=\"{controller}\" asp-action=\"{action}\" asp-route-subset=\"{ViewContext.RouteData.Values["subset"]}\">Home (generated)</a>";
}