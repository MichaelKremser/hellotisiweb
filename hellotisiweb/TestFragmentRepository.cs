using mkcs.libtisiweb.Factory;
using mkcs.libtisiweb.Models;
using mkcs.libtisiweb.Repositories;

namespace hellotisiweb;

internal class TestFragmentRepository : IFragmentRepository
{
    public TestFragmentRepository(IFactory factory)
    {
        _factory = factory;
    }
    
    public string DefaultSubset { get; set; } = string.Empty;

    private readonly Dictionary<string, string> _TestData = new() {
        ["title"] = nameof(TestFragmentRepository),
        ["heading"] = "Welcome at hellotisiweb",
        ["text"] = "This is some sample text"
    };
    private readonly IFactory _factory;

    public int Count => _TestData.Count;

    public IList<Category> Categories => Enumerable.Empty<Category>().ToList();

    public void EnableCaching(TimeSpan cachingTime) => throw new NotImplementedException();

    public string GetFragmentValue(string fragmentName, string fragmentSubset)
        => _TestData.ContainsKey(fragmentName) ? _TestData[fragmentName] : string.Empty;

    public void SetFragmentValue(string fragmentName, string fragmentSubset, string fragmentValue) => throw new NotImplementedException();

    public IFragment GetFragment(string fragmentName)
    {
        var fragment = _factory.CreateFragment(fragmentName);
        fragment.UpdateSubset(DefaultSubset, _TestData[fragmentName]);
        return fragment;
    }
}