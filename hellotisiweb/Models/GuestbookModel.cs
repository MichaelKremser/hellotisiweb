namespace hellotisiweb.Models;

using mkcs.libtisiweb.ViewModels.Page;
using mkcs.libtisiweb.ViewModels.Content;

public record GuestbookModel : DefaultModel
{
    public int PageNumber { get; set;} = 0;

    public int PageCount { get; set;} = 0;

    public IList<GuestbookEntryViewModel> GuestbookEntries = new List<GuestbookEntryViewModel>();
}