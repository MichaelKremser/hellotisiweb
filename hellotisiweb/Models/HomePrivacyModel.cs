namespace hellotisiweb.Models;

public record HomePrivacyModel(string? Title, string? Text);