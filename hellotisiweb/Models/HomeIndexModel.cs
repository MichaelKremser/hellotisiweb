namespace hellotisiweb.Models;

public class HomeIndexModel
{
    public string? Title { get; set; }

    public string? Heading { get; set; }

    public string? Text { get; set; }

    public string? DebugInformation { get; set; }
}