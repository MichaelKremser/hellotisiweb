namespace hellotisiweb.Services;

using mkcs.libtisiweb.Data.Access.Guestbook;
using mkcs.libtisiweb.Data.Entities;

public class GuestbookDataAccess : GuestbookInMemoryDataAccess
{
    public GuestbookDataAccess()
    {
        var fakeAuthors = new string[] { "Michael H Kremser", "Edgar E Poe", "James T Kirk", "Santa Claus", "Easter Rabbit", "Luke Skywalker" };
        for (var entryNr = 1; entryNr < 101; entryNr++)
        {
            var id = Guid.NewGuid();
            var entry = new GuestbookEntry
            {
                ID = id,
                Date = DateTime.Now.AddMinutes(-entryNr),
                Subject = "Test entry " + entryNr,
                Text = $"This is a test entry. It has ID {id} and should appear on the page.",
                AuthorName = fakeAuthors[Random.Shared.Next(0, fakeAuthors.Length)]
            };
            _entries.Add(entry);
        }
    }
}