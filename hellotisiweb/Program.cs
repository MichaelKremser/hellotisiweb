namespace hellotisiweb;

using System.Diagnostics;
using mkcs.libtisiweb;
using mkcs.libtisiweb.Factory;
using mkcs.libtisiweb.Repositories;
using mkcs.libtisiweb.Repositories.Enums;
using mkcs.libtisiweb.Data.Access.Guestbook;
using hellotisiweb.Services;
using Microsoft.AspNetCore.HttpOverrides;
using hellotisiweb.Helpers;
using mkcs.libtisiweb.Models;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.
        builder.Services.AddControllersWithViews();
        //builder.Services.AddSingleton<ITisiwebAppService, TisiwebAppService>();
        builder.Services.AddSingleton(typeof(ITisiwebAppService), CreateAppService(builder.Services));
        // builder.Services.AddHttpContextAccessor();
        // builder.Services.AddTransient<IActionContextAccessor, ActionContextAccessor>();

        var app = builder.Build();

        app.UseForwardedHeaders(new ForwardedHeadersOptions
        {
            ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
        });

        // Configure the HTTP request pipeline.
        if (!app.Environment.IsDevelopment())
        {
            app.UseExceptionHandler("/Home/Error");
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }

        // app.Use(async (context, next) =>
        // {
        //     Console.WriteLine($"QueryString={context.Request.QueryString}");
        //     var routeData = context.Request.RouteValues;
        //     var subset = routeData["subset"] ?? "??";
        //     Console.WriteLine($"{nameof(subset)}={subset}");

        //     await next();
        // });

        //app.UseHttpsRedirection();
        app.UseStaticFiles();

        app.UseRouting();

        app.UseAuthorization();

        app.MapControllerRoute(
            name: "default",
            pattern: "{controller=Home}/{action=Index}/{subset=en}/{id?}");

        app.Run();
    }

    private static ITisiwebAppService CreateAppService(IServiceCollection services)
    {
        Trace.TraceInformation($"Running {nameof(CreateAppService)}");
        var tisiWebAppService = new TisiwebAppService();
        tisiWebAppService.RepositoryManager.DefaultSubset = "en";
        tisiWebAppService.RootApplicationDirectory = Environment.CurrentDirectory;
        tisiWebAppService.WebRootDirectory = Path.Combine(tisiWebAppService.RootApplicationDirectory, "wwwroot");
        tisiWebAppService.RegisterRepositoryByUri(Constants.MainRepositoryId, Path.Combine(tisiWebAppService.WebRootDirectory, "data", "hellotisiweb_mainrepo.xml"));
        tisiWebAppService.RegisterRepositoryInstance("test", new TestFragmentRepository(tisiWebAppService.Factory), CachingStrategy.None);
        tisiWebAppService.RepositoryManager.AddCustomCategories(AddSubCategories);
        services.AddSingleton(typeof(IRepositoryManager), tisiWebAppService.RepositoryManager);
        services.AddScoped<IMediaRepository, MediaRepository>();
        services.AddSingleton(typeof(IFactory), tisiWebAppService.Factory);
        services.AddSingleton(typeof(IGuestbookDataAccess), new GuestbookDataAccess());
        services.AddTransient<TisiWebHtmlHelper<dynamic>>();
        return tisiWebAppService;
    }

    private static void AddSubCategories(Category category)
    {
        Trace.WriteLine($"{category} added");
        if (category.Controller == "Pictures")
        {
            foreach (var year in new[] { 2014, 2023})
            {
                category.Categories.Add(new Category("Pictures", "Index", year.ToString()) { Description = new SimpleTextFragment("Pictures.Year" + year.ToString(), year.ToString()), IsActive = false });
            }
            Trace.WriteLine($"Added subcategories to {category}");
        }
    }
}