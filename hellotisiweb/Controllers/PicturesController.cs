using mkcs.libtisiweb;
using mkcs.libtisiweb.Repositories;
using mkcs.libtisiweb.Controllers;

namespace hellotisiweb.Controllers;

public class PicturesController : MediaController
{
    private readonly ILogger<PicturesController> _logger;
    private readonly ITisiwebAppService _tisiwebAppService;
    private readonly IMediaRepository _mediaRepository;

    protected override bool HandlesPictures => true;
    protected override string LocalFragmentRepositoryFile => "pictures.xml";
    protected override string LocalFragmentRepositoryNamespace => "Pictures.Details";
    protected override string MediaRootFolder => "~/pics";

    public PicturesController(ILogger<PicturesController> logger, ITisiwebAppService tisiwebAppService, IMediaRepository mediaRepository)
        : base(logger, tisiwebAppService, mediaRepository, Constants.MainRepositoryId)
    {
        _logger = logger;
        _tisiwebAppService = tisiwebAppService ?? throw new ArgumentNullException(nameof(tisiwebAppService));
        _mediaRepository = mediaRepository ?? throw new ArgumentNullException(nameof(mediaRepository));
        _logger.LogInformation(nameof(PicturesController) + " ctor executed");
    }
}