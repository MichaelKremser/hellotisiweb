﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using hellotisiweb.Models;
using mkcs.libtisiweb;
using mkcs.libtisiweb.Repositories;
using Microsoft.AspNetCore.Mvc.Filters;

namespace hellotisiweb.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly ITisiwebAppService _tisiwebAppService;
    private readonly IRepositoryManager _repositoryManager;
    private string _subset = string.Empty;

    public HomeController(ILogger<HomeController> logger, ITisiwebAppService tisiwebAppService)
    {
        _logger = logger;
        _tisiwebAppService = tisiwebAppService ?? throw new ArgumentNullException(nameof(tisiwebAppService));
        _repositoryManager = _tisiwebAppService.RepositoryManager;
    }

    public override void OnActionExecuting(ActionExecutingContext context)
    {
        if (string.IsNullOrEmpty(_subset))
        {
            _subset = _tisiwebAppService.GetURIParameter(context.RouteData.Values, "subset");
        }
    }

    public IActionResult Index()
    {
        HomeIndexModel homeIndexModel = new() {
            Heading = _repositoryManager.GetFragmentValueFromRepository("test", "heading", string.Empty),
            Title = _repositoryManager.GetFragmentValueFromRepository("test", "title", string.Empty),
            Text = _repositoryManager.GetFragmentValueFromRepository("test", "text", string.Empty),
            DebugInformation = string.Join("<br>", _repositoryManager.GetCategories())
        };
        return View(homeIndexModel);
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
