using Microsoft.AspNetCore.Mvc;
using mkcs.libtisiweb;
using mkcs.libtisiweb.Controllers;
using mkcs.libtisiweb.ViewModels.Content;
using mkcs.libtisiweb.Data.Access.Guestbook;
using hellotisiweb.Models;

namespace hellotisiweb.Controllers;

public class GuestbookController : TisiController
{
    private readonly ILogger<GuestbookController> _logger;
    private readonly ITisiwebAppService _tisiwebAppService;
    private readonly IGuestbookDataAccess _guestbookDataAccess;
    private readonly int _pageSize = 10;

    public GuestbookController(ILogger<GuestbookController> logger, ITisiwebAppService tisiwebAppService, IGuestbookDataAccess guestbookDataAccess)
        : base(tisiwebAppService, Constants.MainRepositoryId)
    {
        _logger = logger;
        _tisiwebAppService = tisiwebAppService ?? throw new ArgumentNullException(nameof(tisiwebAppService));
        _guestbookDataAccess = guestbookDataAccess ?? throw new ArgumentNullException(nameof(guestbookDataAccess));
        _logger.LogInformation(nameof(GuestbookController) + " ctor executed");
    }
    
    public IActionResult Index(int pageNumber = 1)
    {
        _logger.LogInformation($"{nameof(GuestbookController)}.{nameof(Index)}({pageNumber})");
        var model = HandleBasicFragments<GuestbookModel>(writeViewData: true);
        model.PageNumber = pageNumber;
        model.PageCount = _guestbookDataAccess.GetCount(Guid.Empty) / _pageSize;
        var entries = _guestbookDataAccess.GetEntries(Guid.Empty, (pageNumber - 1) * _pageSize, _pageSize);
        foreach (var entry in entries)
        {
            var vmEntry = new GuestbookEntryViewModel
            {
                ID = entry.ID.ToString(),
                Subject = entry.Subject,
                Text = entry.Text,
                Date = entry.Date,
                Author = entry.AuthorName
            };
            model.GuestbookEntries.Add(vmEntry);
        }
        return View(model);
    }
}