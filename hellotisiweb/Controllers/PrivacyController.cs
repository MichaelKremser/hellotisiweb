using hellotisiweb.Models;
using Microsoft.AspNetCore.Mvc;
using mkcs.libtisiweb;
using mkcs.libtisiweb.Controllers;
using mkcs.libtisiweb.Repositories;

namespace hellotisiweb.Controllers;

public class PrivacyController : TisiController
{
    private readonly ILogger<PrivacyController> _logger;
    private readonly ITisiwebAppService _tisiwebAppService;
    private readonly IRepositoryManager _repositoryManager;

    public PrivacyController(ILogger<PrivacyController> logger, ITisiwebAppService tisiwebAppService)
        : base(tisiwebAppService, Constants.MainRepositoryId)
    {
        _logger = logger;
        _tisiwebAppService = tisiwebAppService ?? throw new ArgumentNullException(nameof(tisiwebAppService));
        _repositoryManager = _tisiwebAppService.RepositoryManager;
        _logger.LogInformation(nameof(PrivacyController) + " ctor executed");
    }

    private string GetFragmentValue(string fragmentName) => _repositoryManager.GetFragmentValueFromRepository(Constants.MainRepositoryId, "Privacy.Index." + fragmentName, Subset);

    public IActionResult Index()
    {
        HomePrivacyModel homePrivacyModel = new(
            GetFragmentValue("title"),
            GetFragmentValue("text")
        );
        return View(homePrivacyModel);
    }
}